package proxy;

/**
 * Clase abstracta que representa a todos los proxys
 * @author Agustin Antoine
 *
 */
public abstract class AbstractProxy implements IProxy{
	
	protected IProxy prev;
	
	/**
	 * Constructor abstracto que exige a quienes extiendan AbstractProxy a determinar
	 * el proxy previo
	 * @param prev Proxy previo a especificar
	 */
	public AbstractProxy(IProxy prev){
		setPrev(prev);
	}
	
	/**
	 * Metodo que especifica el proxy previo a este
	 */
	protected void setPrev(IProxy n) {
		this.prev = n;
	}
	
}
