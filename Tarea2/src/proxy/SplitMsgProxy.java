package proxy;


/**
 * Proxy que restringe el acceso de los decoradores al nombre de usuario
 * @author Agustin Antoine
 * 
 */
public class SplitMsgProxy extends AbstractProxy {

	/**
	 * Constructor de la clase
	 * 
	 * @param prev  IProxy a seleccionar como previo
	 */
	public SplitMsgProxy(IProxy prev) {
		super(prev);
	}

	/**
	 * Metodo que separa el mensaje en username-msg para que los
	 * decorators trabajen solo sobre el mensaje. Si el user es nulo, se retorna
	 * el mismo mensaje sin modificar ya que este corresonde a un mensaje del
	 * servidor y los decoradores no deben interferirlo 
	 */
	@Override
	public String preProcess(String msg) {
		int index = msg.indexOf("-");
		String username = msg.substring(0, index + 1);
		if (username.equals(""))
			return msg;
		return msg.substring(0, index + 1)+prev.preProcess(msg.substring(index + 1, msg.length()));
	}
	

	public void setPrev(IProxy prev){
		super.setPrev(prev);
	}

}
