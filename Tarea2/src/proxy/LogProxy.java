package proxy;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * Proxy encargado de guardar los mensajes de cada usuario en el archivo
 * ichat.log
 * 
 * @author Agustin Antoine
 * 
 */
public class LogProxy extends AbstractProxy {

	private String file = "ichat.log";

	/**
	 * Constructor de la Clase
	 * 
	 * @param prev
	 *            proxy previo a especificar
	 */
	public LogProxy(IProxy prev) {
		super(prev);

	}

	/**
	 * 
	 */
	@Override
	public String preProcess(String msg) {
		msg = prev.preProcess(msg);
		try {
			PrintStream p = new PrintStream(new BufferedOutputStream(
					new FileOutputStream(file, true)));
			append(p, msg);

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(file);
		}
		return msg;
	}

	public void append(PrintStream p, String content) {
		int index = content.indexOf("-");
		if(index==-1){
			p.println("Server: " + "," + content + ", "
					+ (new java.util.Date().toString()));
		}
		else{
			String username = content.substring(0, index);
			String msg = content.substring(index + 1, content.length());
	
			p.println(username + "," + msg + ", "
					+ (new java.util.Date().toString()));
		}
		p.close();
	}

}
