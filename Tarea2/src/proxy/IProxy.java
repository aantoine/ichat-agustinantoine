package proxy;
/**
 * Interfaz para reconocer los objetos del tipo Proxy
 * @author Agustin Antoine
 *
 */
public interface IProxy {
	/**
	 * Metodo de los Proxy's para modificar un String msg de entrada
	 * @param msg Mensaje de entrada a procesar
	 * @return String obtenido de msg una vez procesado
	 */
	public String preProcess(String msg);

}
