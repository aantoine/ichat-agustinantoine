package proxy;


/**
 * Proxy Nulo, dado un mensaje lo retorna sin modificarlo.
 * @author Agustin Antoine
 *
 */
public class NullProxy implements IProxy {

	/**
	 * Metodo que retorna el mismo parámetro de entrada
	 * @param msg String a devolver
	 * @return String sin alterar
	 */
	@Override
	public String preProcess(String msg) {
		return msg;
	}

}
