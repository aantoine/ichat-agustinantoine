package cc3002.ichat.core;
import java.net.*;
import java.io.*;
import java.util.*;

/**
 * Clase que maneja los clientes asociados a un servidor
 * @author Agustin Antoine
 *
 */
public class ChatHandler extends Thread {
	
	protected static Vector<ChatHandler> handlers = new Vector<ChatHandler>();
	protected Socket socket;
	protected DataInputStream input;
	protected DataOutputStream output;
	private String username;
	
	/**
	 * Constructor de la clase
	 * @param socket Socket que determina la conexion al cliente
	 * @throws IOException
	 */
	public ChatHandler (Socket socket) throws IOException {
		this.socket = socket;
		username="anonymous";
		input = new DataInputStream (new BufferedInputStream (socket.getInputStream ()));
		output = new DataOutputStream (new BufferedOutputStream (socket.getOutputStream ()));
	}

	/**
	 * Metodo que se ejecuta al lanzarse el thread del Handler. Al conectarse un
	 * cliente es agregado al arreglo de clientes, se env�a un mensaje a los demas
	 * clientes conectados anunciando la nueva conexion.
	 * Por cada mensaje recivido del cliente, es enviado por broadcast a los demas
	 */
	public void run () {
		try {
			//first message is the user name
			
			handlers.addElement (this);
			username = input.readUTF().toString();	
			System.out.println(username+" has joined");
			broadcast (username + " has joined.");
			while (true) {
				String msg = input.readUTF ();
				broadcast (username + " - " + msg);
			}
		} catch (IOException ex) {
			//ex.printStackTrace ();
		} finally {
			handlers.removeElement (this);
			broadcast (username + " has left.");
			try {
				socket.close ();
			} catch (IOException ex) {ex.printStackTrace();}
		}
	}

	/**
	 * Metodo que envia a Todos los clientes conectados al server un mensaje 
	 * @param message String a enviar por broadcast
	 */
	protected static void broadcast (String message) {
		synchronized (handlers) {
			Enumeration<ChatHandler> e = handlers.elements ();
			while (e.hasMoreElements ()) {
				ChatHandler handler = e.nextElement ();
				try {
					synchronized (handler.output) {
						handler.output.writeUTF (message);
					}
					handler.output.flush ();
				} catch (IOException ex) {
					handler.stop ();
				}
			}
		}
	}
}
