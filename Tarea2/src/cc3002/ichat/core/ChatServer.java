package cc3002.ichat.core;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Clase que especifica un ChatServer
 * @author Agustin Antoine
 *
 */
public class ChatServer implements Runnable{
	private int port;
	
	/**
	 * Constructor de la Clase
	 * @param port Int puerto al cual conectarse
	 */
	public ChatServer(int port){
		this.port= port;
	}
	
	/**
	 * Metodo que determina la ejecución en paralelo de un Server, al aceptar
	 * una nueva conexion agrega el cliente (socket) a un ChatHandler 
	 */
	@Override
	public void run() {
		try {
			ServerSocket server = new ServerSocket (port);
			while (true) {
				Socket client = server.accept ();
				System.out.println ("Accepted from " + client.getInetAddress ());
				ChatHandler c = new ChatHandler (client);
				c.start ();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Main de la clase, crea y ejecuta un nuevo Server en el puerto definido
	 * @param args String[] de parámetros recividos por consola
	 * @throws IOException
	 */
	public static void main (String args[]) throws IOException {
		int port= Integer.parseInt (args[0]);

	    new ChatServer (port).run();
	  }
}
