package cc3002.ichat.core;

import java.net.*;
import java.util.Observable;
import java.io.*;

/**
 * Clase que representa un Cliente del Chat
 * @author Agustin Antoine
 *
 */
public class ChatClient extends Observable implements Runnable {
	
	private String user;
	private DataInputStream input;
	private DataOutputStream output;
	private Thread listener;

	/**
	 * Constructor de la clase, incializa los parametros y crea el thread que 
	 * mantendra al cliente
	 * @param user String nombre de usuario
	 * @param i InputStream en entrada al cliente
	 * @param o OutputStream de salida al cliente
	 */
	public ChatClient (String user,InputStream i, OutputStream o) {
		this.user=user;
		this.input = new DataInputStream (new BufferedInputStream (i));
		this.output = new DataOutputStream (new BufferedOutputStream (o));
		listener = new Thread (this);
		listener.start ();
		
	}
	
	/**
	 * Funci�n que retorna el username asociado a este chat
	 * @return String username
	 */
	public String username(){
		return user;
	}

	
	/**
	 * Metodo que se ejcuta al inicializarce el thread que corre el cliente.
	 * Al iniciarse envia un mensaje al servidor con el nombre del usuario de
	 * este cliente, luego espera a leer un mensaje desde el servidor en cuyo 
	 * caso notifica a la ventana de chat que llego un mensaje
	 */
	public void run () {
		try {
			sendMessage(user);
			while (true) { 
				setChanged();
				notifyObservers(input.readUTF());}
		} catch (IOException ex) { 
			ex.printStackTrace ();
		} finally {
			listener = null;
			try {output.close (); } 
			catch (IOException ex) {ex.printStackTrace ();}
		}
	}
	
	/**
	 * Metodo para enviar un mensaje al servidor
	 * @param message String mensaje a enviar
	 */
	public void sendMessage(String message){
		try {
			output.writeUTF(message);
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo que cierra la sesion del usuario
	 */
	public void closeSession(){
		if (listener != null)
	        listener.stop ();
	}

	public static void main (String args[]) throws IOException {
		String host="localhost";
		int port=2030;
		if (args.length >1){
			host=args[0];
			port=Integer.parseInt(args[1]);
		}

		Socket socket = new Socket (host, port);
		new ChatClient ("juampi",socket.getInputStream (), socket.getOutputStream ());
	}
}