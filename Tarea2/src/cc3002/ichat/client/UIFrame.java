package cc3002.ichat.client;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.text.html.*;

/** 
 * Frame que representa la ventana de chat
 * @author Agustin Antoine
 *
 */
public class UIFrame extends JFrame{
	
	private JPanel messageList;
	private JTextArea textArea;
	private JButton sendButton;
	private JScrollPane scroll;
	
	/**
	 * Constructor de la clase, crea la venta y agrega los ActionListeners para
	 * poder enviar el mensaje apretando el boton "Enviar" o apretando la tecla 
	 * Enter
	 * @param username String nombre del usuario de esta ventana de chat
	 */
	public UIFrame(String username){
		super(username);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {close();}
		});
		setLayout(new BorderLayout());
		messageList=new JPanel();
		messageList.setLayout(null);
		Dimension size=new Dimension(400,400);
		messageList.setMinimumSize(size);
		messageList.setMinimumSize(size);
		messageList.setPreferredSize(size);
		
		Action send = new AbstractAction("Send") {

		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	send(textArea);
		    }
		};
		
		
		textArea=new JTextArea();
		textArea.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		sendButton=new JButton(send);
		
		this.getRootPane().setDefaultButton(sendButton);
		
		textArea.getInputMap().put(KeyStroke.getKeyStroke("ENTER"),
				"doSomething");
		textArea.getActionMap().put("doSomething",
				send);

		
		JPanel bottomPanel=new JPanel();
		bottomPanel.setLayout(new BorderLayout());
		bottomPanel.add(textArea,BorderLayout.CENTER);
		bottomPanel.add(sendButton,BorderLayout.EAST);
		scroll=new JScrollPane(messageList);
		scroll.setAutoscrolls(true);
		add(scroll,BorderLayout.CENTER);
		add(bottomPanel,BorderLayout.SOUTH);
		pack();
	}
	
	/**
	 * Agrega un texto en Html a la ventana
	 * @param username String nombre del usuario que envia el mensaje
	 * @param html String mensaje a mostrar
	 */
	public void addHtmlMessage(String username,String html){
		JEditorPane editor = new JEditorPane("text/html","<html><body>"+html+"</body></html>");
		editor.setEditable(false);
		addComponent(new UIItem(username,editor));
	}
	
	
	/**
	 * Agrega un nuevo Componente a la pantalla, este corresponde a un mensaje
	 * recivido
	 * @param component UIItem con el mensaje en Html
	 */
	public void addComponent(JComponent component){
		int y=5;
		for(Component c: messageList.getComponents()){
			y+=c.getPreferredSize().getHeight()+2;
		}
		
		component.setBounds(5,y, 380, (int) component.getPreferredSize().getHeight());
		Dimension size=new Dimension(380,(int)(y+component.getPreferredSize().getHeight()+30));
		messageList.setPreferredSize(size);
		messageList.add(component);
		scroll.updateUI();
		JScrollBar vertical = scroll.getVerticalScrollBar();
		vertical.setValue( vertical.getMaximum()+10 );
	}
	
	/**
	 * Envia un mensaje al servidor
	 * @param text JTextArea componente con el mensaje a enviar
	 */
	public void send(JTextArea text){
		textArea.setText("");
	}
	
	/**
	 * Metodo para actualizar la ventana
	 */
	public void updateUI(){
		messageList.updateUI();
	}
	
	/**
	 * Metodo para cerrar la ventana
	 */
	public void close(){
		setVisible(false);
		System.exit(0);
	}
}
