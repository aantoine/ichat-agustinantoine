package cc3002.ichat.client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JTextArea;


import proxy.IProxy;
import proxy.LogProxy;
import proxy.NullProxy;
import proxy.SplitMsgProxy;

import cc3002.ichat.core.*;
import decorator.BWFDecorator;
import decorator.EPDecorator;
import decorator.HLDecorator;
import decorator.MSCDecorator;
import decorator.SRCDecorator;
import factory.AbstractFactory;
import factory.ReceiveFactory;
import factory.SendFactory;

public class UIChatGroup extends UIFrame implements Observer{
	private ChatClient client;
	private static IProxy receiveChain;
	private static IProxy sendChain;
	
	public UIChatGroup(ChatClient client){
		super(client.username());
		sendChain = new NullProxy();
		receiveChain = new NullProxy();
		client.addObserver(this);
		this.client=client;	
	}
	
	/**
	 * Este metodo se llama cada ves que alguien manda un mensaje al server
	 * @param o es un objeto ChatClient que recibio el mensaje del server
	 * @param arg es el mensaje en si
	 * 
	 * arg es de tipo String con el siguiente formato 
	 * username - message
	 */
	@Override
	public void update(Observable o, Object arg) {
		String message=receiveChain.preProcess((String) arg);
		int index=message.indexOf("-");
		String username= message.substring(0,index+1);
		message=message.substring(index + 1, message.length());
		
		//agregando el mensaje a la ventana
		addHtmlMessage(username, message);
		//actualizando la ventana
		updateUI();
	}
	/**
	 * Es metodo se invoca cuando alguien presiona el boton enviar
	 * @param text es el objeto que tiene dentro el texto escrito por el usuario
	 */
	@Override
	public void send(JTextArea text) {
		String msg = sendChain.preProcess(text.getText());
		client.sendMessage(msg);
		super.send(text);
	}
	/**
	 * Este metodo se llama cuando alguien cierra la ventana
	 */
	@Override
	public void close() {
		// cerrando la sesion con el server
		client.closeSession();
		super.close();
	}
	
	public static void main(String[] args) throws UnknownHostException, IOException {
			
		String host=args[0];
		int port=Integer.parseInt(args[1]);
		String username=args[2];

		Socket socket = new Socket (host, port);
		ChatClient  client=new ChatClient (username,socket.getInputStream (), socket.getOutputStream ());
		UIChatGroup ui=new UIChatGroup(client);
		
		receiveChain = new ReceiveFactory().getChain(args); 
		sendChain = new SendFactory().getChain(args);
		
		
		ui.setVisible(true);
	}
}
