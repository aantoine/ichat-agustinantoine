package cc3002.ichat.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

/**
 * Clase que representa un mensaje en el chat
 * @author Agustin Antoine
 *
 */
public class UIItem extends JPanel{

	private JLabel username;
	private JComponent component;
	/**
	 * Constructor de la clase, establece el nommbre de usaurio y 
	 * el msanje en html
	 * @param username String nombre de usuario
	 * @param component Editor con el texto en Html
	 */
	public UIItem(String username,JComponent component){
		this.username=new JLabel(username);
		this.component=component;
		setLayout(new BorderLayout());
		add(this.username,BorderLayout.WEST);
		add(this.component,BorderLayout.CENTER);
		//setBackground(Color.WHITE);
		setOpaque(false);
	}
}
