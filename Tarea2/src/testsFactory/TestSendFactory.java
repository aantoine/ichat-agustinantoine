package testsFactory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import factory.SendFactory;

import proxy.IProxy;

public class TestSendFactory {

	@Test
	public void testNoneEntry() {
		String[] set = {};
		IProxy sendChain = new SendFactory().getChain(set);

		String original = "Hola Mundo!";
		assertEquals(original, sendChain.preProcess(original));

	}

	@Test
	public void testWrongEntry() {
		String[] set = { "", "", "", "--hola", "" };
		IProxy sendChain = new SendFactory().getChain(set);

		String original = "Hola Mundo!";
		assertEquals(original, sendChain.preProcess(original));

	}

	@Test
	public void testAll() {
		String[] set = { "", "", "", "-all" };
		IProxy sendChain = new SendFactory().getChain(set);

		String original = "Hola Mundo!";
		assertEquals(original, sendChain.preProcess(original));

		original = "code hola";
		assertEquals("<code>hola</code>", sendChain.preProcess(original));

		original = "$hola.png$";
		assertEquals("<img src=\"file:images/hola.png\" width=50 height=50/>",
				sendChain.preProcess(original));

		original = "code $hola$";
		assertEquals("<code>$hola$</code>", sendChain.preProcess(original));
	}

	// @Test
	// public void testReceive(){
	// abstractFactory.add("--msc");
	// IProxy receiveChain = abstractFactory.getReceive();
	//
	// String original = "juampi - colores";
	// assertEquals("juampi - <u><font color=\"red\">colores</font></u>",
	// receiveChain.preProcess(original));
	//
	//
	// abstractFactory.add("--bwf");
	// abstractFactory.add("--hl");
	// abstractFactory.add("--log");
	// receiveChain = abstractFactory.getReceive();
	//
	//
	//
	// }

	@Test
	public void testSend() {
		String[] set = { "", "", "", "--src", "--ep" };
		IProxy sendChain = new SendFactory().getChain(set);
		String original = "Hola Mundo!";
		assertEquals(original, sendChain.preProcess(original));

		original = "code hola";
		assertEquals("<code>hola</code>", sendChain.preProcess(original));

		original = "$hola.png$";
		assertEquals("<img src=\"file:images/hola.png\" width=50 height=50/>",
				sendChain.preProcess(original));

		original = "code $hola$";
		assertEquals("<code>$hola$</code>", sendChain.preProcess(original));
	}

	@Test
	public void testDefault() {
		String[] set = { "", "", "", "-default" };
		IProxy sendChain = new SendFactory().getChain(set);
	}
}
