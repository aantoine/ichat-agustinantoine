package testsFactory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import factory.ReceiveFactory;
import factory.SendFactory;

import proxy.IProxy;

public class TestReceiveFactory {

	@Test
	public void testNoneEntry() {
		String[] set = {};
		IProxy receiveChain = new ReceiveFactory().getChain(set);

		String original = "Hola Mundo!";
		assertEquals(original, receiveChain.preProcess(original));

	}

	@Test
	public void testWrongEntry() {
		String[] set = { "", "", "", "--hola", "" };
		IProxy receiveChain = new ReceiveFactory().getChain(set);

		String original = "Hola Mundo!";
		assertEquals(original, receiveChain.preProcess(original));

	}

	@Test
	public void testAll() {
		String[] set = { "", "", "", "-all" };
		IProxy receiveChain = new ReceiveFactory().getChain(set);

		String original = "Hola Mundo!";
		assertEquals(original, receiveChain.preProcess(original));

		original = "juampi - colores";
		assertEquals("juampi - <u><font color=\"red\">colores</font></u>",
				receiveChain.preProcess(original));

	}

	@Test
	public void testReceive() {
		String[] set = { "", "", "", "--msc" };
		IProxy receiveChain = new ReceiveFactory().getChain(set);

		String original = "juampi - colores";
		assertEquals("juampi - <u><font color=\"red\">colores</font></u>",
				receiveChain.preProcess(original));

		String[] set2 = { "", "", "", "--msc", "--bwf", "--log","--hl" };
		receiveChain = new ReceiveFactory().getChain(set2);

	}

	@Test
	public void testDefault() {
		String[] set = { "", "", "", "-default" };
		IProxy receiveChain = new ReceiveFactory().getChain(set);
	}
}
