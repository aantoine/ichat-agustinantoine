package factory;

import decorator.EPDecorator;
import decorator.SRCDecorator;
import proxy.IProxy;
import proxy.NullProxy;

/**
 * Clase encargada de construir la cadena de proxys de salida
 * @author Agustin Antoine
 *
 */
public class SendFactory extends AbstractFactory{
	private IProxy sendChain;
	
	/**
	 * Constructor de la clase, crea los plugins a utilizar por defecto
	 * sendChain es la cadena de salida
	 */
	public SendFactory(){
		sendChain = new NullProxy();
		}
	
	
	@Override
	protected void add(String atr){
		if (atr.equals("--ep")) sendChain = new EPDecorator(sendChain);
		else if (atr.equals("--src")) sendChain = new SRCDecorator(sendChain);
		else if (atr.equals("-all")){
			sendChain = new SRCDecorator(new EPDecorator(sendChain));
		}
		else if (atr.equals("-default")){
			sendChain = new EPDecorator(sendChain);
		}
	}
	
	@Override
	public IProxy getChain(String[] args){
		add(args);
		return sendChain;
	}
	

}
