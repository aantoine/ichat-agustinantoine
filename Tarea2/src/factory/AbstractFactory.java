package factory;

import proxy.IProxy;


/**
 * Clase Factory Abstract de la que extienden las Factory para las cadenas de plugins
 * @author Agustin Antoine
 *
 */
public abstract class AbstractFactory {

	/**
	 * Metodo para agregar todos los comandos entregados en el arreglo de Strings recivido
	 * @param args Arreglo de Strings con los comandos ingresados
	 */
	protected void add(String[] args){
		for (int k=3;k<args.length;k++){
			add(args[k]);
		}
	}
	
	/**
	 * Metodo que agrega un Plugin a la cadena de proxys dado un comando
	 * @param args comando que determina el plugin a agregar en la cadena
	 */
	protected abstract void add(String args);
	
	
	/**
	 * Metodo que retorna la cadena de proxys construida dado un arreglo de comandos
	 * @param args Arreglo de comandos de salida
	 * @return Cadena de salida
	 */
	public abstract IProxy getChain(String[] args);
	

}
