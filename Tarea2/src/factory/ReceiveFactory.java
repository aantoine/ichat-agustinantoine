package factory;

import decorator.BWFDecorator;
import decorator.HLDecorator;
import decorator.MSCDecorator;
import proxy.IProxy;
import proxy.LogProxy;
import proxy.NullProxy;
import proxy.SplitMsgProxy;

/**
 * Clase encargada de construir la cadena de proxys de entrada
 * @author Agustin Antoine
 *
 */
public class ReceiveFactory extends AbstractFactory{
	private IProxy decoratorsChain;
	private IProxy receiveChain;
	private SplitMsgProxy split;
	
	/**
	 * Constructor de la clase, crea los plugins a utilizar por defecto
	 * receiveChain es la cadena a entregar
	 * split es el Proxy que sirve de puente entre Decorators y otros Proxys
	 * decoratorsChains es la cadena de Decorators
	 */
	public ReceiveFactory(){
		decoratorsChain = new NullProxy();
		split = new SplitMsgProxy(new NullProxy());
		receiveChain = split;
	}

	@Override
	protected void add(String atr) {

		if (atr.equals("--msc")) decoratorsChain = new MSCDecorator(decoratorsChain); 
		else if (atr.equals("--bwf")) decoratorsChain = new BWFDecorator(decoratorsChain); 
		else if (atr.equals("--hl")) decoratorsChain = new HLDecorator(decoratorsChain);
		else if (atr.equals("--log")) receiveChain = new LogProxy(split);
		else if (atr.equals("-all")){
			receiveChain = new LogProxy(receiveChain);
			decoratorsChain = new MSCDecorator(new BWFDecorator(new HLDecorator(decoratorsChain)));
		}
		else if (atr.equals("-default")){
			receiveChain = new LogProxy(receiveChain);
			decoratorsChain = new BWFDecorator(new HLDecorator(decoratorsChain));
		}
		
	}

	@Override
	public IProxy getChain(String[] args) {
		add(args);
		split.setPrev(decoratorsChain);
		return receiveChain;
	}

}
