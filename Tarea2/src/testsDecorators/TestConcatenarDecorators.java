package testsDecorators;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import proxy.IProxy;
import proxy.NullProxy;

import decorator.*;


public class TestConcatenarDecorators {

	IProxy nullProxy;
	IProxy proxy;

	@Before
	public void setUp() { // create the test data
		nullProxy = new NullProxy();
	}

	@Test
	public void testBWFandMSC() {
		proxy = new MSCDecorator(nullProxy);
		proxy = new BWFDecorator(proxy);
		
		String original="<b>La puta red !!</b>";
		String m1 = "<u><font color=\"red\">La</font></u>";
		String m2 = "<u><font color=\"red\">&#$@*</font></u>";
		String m3 = "<u><font color=\"red\">!!</font></u>";
		String expected="<b>"+m1+" "+m2+" red "+m3+"</b>";
		assertEquals(expected, proxy.preProcess(original));		
		
		proxy = new BWFDecorator(nullProxy);
		proxy = new MSCDecorator(proxy);
		
		assertEquals(expected, proxy.preProcess(original));
	}
	
	@Test
	public void testEPandMSC() {
		proxy = new EPDecorator(nullProxy);
		proxy = new MSCDecorator(proxy);
		String original="red blue plas,  $plas.png$ ";
		String expected="red blue <u><font color=\"red\">plas</font></u>,  <img src=\"file:images/plas.png\" width=50 height=50/> ";
		assertEquals(expected, proxy.preProcess(original));
		
	}

}
