package testsDecorators;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import proxy.IProxy;
import proxy.NullProxy;


import decorator.HLDecorator;

public class TestHashtagLabel {
	IProxy proxy;

	@Before
	public void setUp() { // create the test data
		IProxy n = new NullProxy();
		proxy = new HLDecorator(n);
	}

	@Test
	public void testPreProcess() {
		String original = "Yayita, your #POO instincts are growing. Do you have any questions for @juampi today?";
		String expected = "Yayita, your <em>#POO</em> instincts are growing. Do you have any questions for <em>@juampi</em> today?";
		assertEquals(expected, proxy.preProcess(original));
	}
	
	@Test
	public void testPreProcess2() {
		String original = "No escribo con Tags ni Labels";
		assertEquals(original, proxy.preProcess(original));
	}
	
	@Test
	public void testPreProcessTags() {
		String original = "<b> Hola</b>";
		assertEquals(original, proxy.preProcess(original));
		
		String original2 = "<b> Hola @antoine </b>";
		String expected2 = "<b> Hola <em>@antoine</em> </b>";
		assertEquals(expected2, proxy.preProcess(original2));
	}

}
