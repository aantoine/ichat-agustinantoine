package testsDecorators;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import proxy.IProxy;
import proxy.NullProxy;

import decorator.SRCDecorator;

public class TestCode {

	IProxy proxy;

	@Before
	public void setUp() { // create the test data
		IProxy N = new NullProxy();
		proxy = new SRCDecorator(N);
	}

	@Test
	public void testPreProcess() {
		
		String original = "code tamagotchi.doSport()";
		String expected = "<code>tamagotchi.doSport()</code>";
		assertEquals(expected, proxy.preProcess(original));
	}
	
	@Test
	public void testPreProcess2() {
		String original = "No uso codigo";
		assertEquals(original, proxy.preProcess(original));
		String original2 = "hola";
		assertEquals(original2, proxy.preProcess(original2));
	}
	
	@Test
	public void testPreProcessTags() {
		String original = "<b> Hola</b>";
		assertEquals(original, proxy.preProcess(original));
		
		String original2 = "code <b> texto entre html </b>";
		String expected2 = "<code><b> texto entre html </b></code>";
		assertEquals(expected2, proxy.preProcess(original2));
	}
}
