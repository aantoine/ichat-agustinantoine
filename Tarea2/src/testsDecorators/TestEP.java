package testsDecorators;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import proxy.IProxy;
import proxy.NullProxy;

import decorator.EPDecorator;


public class TestEP {

	IProxy proxy;

	@Before
	public void setUp() { // create the test data
		IProxy n = new NullProxy();
		proxy = new EPDecorator(n);
	}

	@Test
	public void testPreProcess() {
		String original = "$auxiliar.png$";
		String expected = "<img src=\"file:images/auxiliar.png\" width=50 height=50/>";
		assertEquals(expected, proxy.preProcess(original));
	}
	
	@Test
	public void testPreProcess2() {
		String original = "hola que hace";
		String expected = "hola que hace";
		assertEquals(expected, proxy.preProcess(original));
	}
	
	@Test
	public void testPreProcess3() {
		String original = "<b>hola que hace $auxiliar.png$</b>";
		String expected = "<b>hola que hace "+
				"<img src=\"file:images/auxiliar.png\" width=50 height=50/></b>";
		assertEquals(expected, proxy.preProcess(original));
	}

}
