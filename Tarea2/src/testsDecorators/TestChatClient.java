package testsDecorators;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.Socket;

import javax.swing.JTextArea;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cc3002.ichat.client.UIChatGroup;
import cc3002.ichat.core.ChatClient;
import cc3002.ichat.core.ChatServer;

public class TestChatClient {

	Thread thread;

	@Before
	public void setUp() throws IOException{
		thread = new Thread(){
		    public void run(){
		    	
		    	new ChatServer (1234).run();}
		  };
		 
		  thread.start();		
	}
	
	@SuppressWarnings("deprecation")
	@After
	public void tearDown(){
		thread.stop();
	}
	
	@Test
	public void test() throws IOException {
		@SuppressWarnings("resource")
		Socket socket = new Socket ("localhost", 1234);
		ChatClient c = new ChatClient ("juampi",socket.getInputStream (), socket.getOutputStream ());
		assertEquals("juampi", c.username());
		
		UIChatGroup Ui = new UIChatGroup(c);
		Ui.send(new JTextArea("hola"));
		Ui.setVisible(true);
		
		c.closeSession();
		
	}
}