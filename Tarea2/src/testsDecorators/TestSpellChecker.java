package testsDecorators;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import proxy.IProxy;
import proxy.NullProxy;

import decorator.MSCDecorator;


public class TestSpellChecker {

	IProxy proxy;

	@Before
	public void setUp() { // create the test data
		IProxy n = new NullProxy();
		proxy = new MSCDecorator(n);
	}

	@Test
	public void testPreProcess() {
		String original = "colores como red black blue";
		String m1 = "<u><font color=\"red\">colores</font></u>";
		String m2 = "como";
		String expected = m1+" "+m2+" "+"red black blue";
		assertEquals(expected, proxy.preProcess(original));
	}
	
	@Test
	public void testPreProcess2() {
		String original = "red black blue perro";
		assertEquals(original, proxy.preProcess(original));
	}
	
	@Test
	public void testPreProcess3() {
		String original = "<b>colores como red black blue</b>";
		String m1 = "<u><font color=\"red\">colores</font></u>";
		String m2 = "como";
		String expected ="<b>"+ m1+" "+m2+" "+"red black blue</b>";
		assertEquals(expected, proxy.preProcess(original));
	}

}
