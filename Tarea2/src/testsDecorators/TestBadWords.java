package testsDecorators;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import proxy.IProxy;
import proxy.NullProxy;

import decorator.BWFDecorator;


public class TestBadWords {

	IProxy proxy;

	@Before
	public void setUp() { // create the test data
		IProxy N = new NullProxy();
		proxy = new BWFDecorator(N);
	}

	@Test
	public void testPreProcess() {
		String original = "La puta que te pareo !!";
		String expected = "La &#$@* que te &#$@* !!";
		assertEquals(expected, proxy.preProcess(original));
	}
	
	@Test
	public void testPreProcess2() {
		String original = "La cosa que te tuvo !!";
		String expected = "La cosa que te tuvo !!";
		assertEquals(expected, proxy.preProcess(original));
	}
	
	@Test
	public void testPreProcessTags() {
		String original = "<b> Hola</b>";
		assertEquals(original, proxy.preProcess(original));
		
		String original2 = "<b> La puta  red !! </b>";
		String expected2 = "<b> La &#$@*  red !! </b>";
		assertEquals(expected2, proxy.preProcess(original2));
	}
}
