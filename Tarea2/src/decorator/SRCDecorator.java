package decorator;

import proxy.IProxy;

public class SRCDecorator extends AbstractDecorator {

	public SRCDecorator(IProxy n) {
		super(n);
	}

	@Override
	public String preProcess(String msg) {
		
		if(condition(msg))
			msg=modify(msg);
		
		return prev.preProcess(msg);
	}

	@Override
	public boolean condition(String msg) { 
		if(msg.length()<5) return false;
		return (msg.substring(0, 5).equals("code "));
	}

	@Override
	public String modify(String msg) {
		return "<code>"+msg.substring(5,msg.length())+"</code>";
	}

}
