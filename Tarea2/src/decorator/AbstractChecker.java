package decorator;

import java.util.ArrayList;

import proxy.IProxy;
/**
 * Clase Abstracta para todos aquellos Proxy's que verifican si cierto String est� o no en un
 *  Arreglo de String's
 * @author Agustin Antoine
 *
 */
public abstract class AbstractChecker extends AbstractDecorator {

	public AbstractChecker(IProxy prev) {
		super(prev);
		list = new ArrayList<String>();
	}

	/**
	 * Arreglo de Strings sobre el cual se ver�fica la condici�n
	 */
	private ArrayList<String> list;
	
	public void addToList(String e){
		list.add(e);
	}
	
	/**
	 * Metodo para verificar si el String pertenece o no al Diccionario
	 * @param msg String sobre el cual consultar la condicion
	 * @return Boolean True si esta en el diccionario
	 */
	public boolean condition(String msg){
		return list.contains(msg);
	}
}
