package decorator;

import proxy.IProxy;
/**
 * Proxy que verifica si la palabra es una "mala palabra" o no
 * @author Agustin Antoine
 *
 */
public class BWFDecorator extends AbstractChecker{

	/**
	 * Construcotr de la clase
	 * @param n Decorador previo
	 */
	public BWFDecorator(IProxy n) {
		super(n);		
		addToList("puta");
		addToList("pareo");
		addToList("ctm");
		addToList("tonto");
		addToList("pesado");	
	}

	/**
	 * Verifica si el String msg est� en el listado de malas palabras 
	 * @param msg String a verificar
	 * @return True si est� entre las malas palabras
	 */
	@Override
	public boolean condition(String msg) {
		return super.condition(msg);
	}

	/**
	 * Modifica el texto cambi�ndolo por un texto establecido
	 * @param msg String a modificar
	 * @return String modificado
	 */
	@Override
	public String modify(String msg) {
		return "&#$@*";
	}

}
