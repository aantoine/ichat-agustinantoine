package decorator;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import proxy.AbstractProxy;
import proxy.IProxy;

/**
 * Clase Abstracta de la que extienden todos los Proxy's no nulos.
 * 
 * @author Agustin Antoine
 * 
 */
public abstract class AbstractDecorator extends AbstractProxy implements IDecorator{

	private String pattern = "(<code>(\\w|\\s|\\W)*</code>)|(<(\"[^\"]*\"|'[^']*'|[^'\">])*>)|(\\s+)|([,?\"'])";

	public AbstractDecorator(IProxy prev) {
		super(prev);
	}

	/**
	 * Metodo que permite procesar un texto con tags html y multiples espacios.
	 * Funciona a base de reconocer patrones por medio de expresiones regulares,
	 * y con esto separar el texto en palabras individuales que analizar.
	 * 
	 * Crea un matcher segun una expresion regular privada (Reconoce html, espacios, 
	 * y signos de puntuacion) y sobre el texto entregado obtiene secunacialmente
	 * los elementos que calzan con el patron.
	 * Aquellos substring que no calcen son procesados para ver si cumplen o no
	 * la condicon de modificaion.
	 * Una vez procesado todo el texto, se reconstruye y es retornado
	 * @param text String a procesar
	 * @return String procesado
	 */
	@Override
	public String preProcess(String text) {
		text = prev.preProcess(text);

		Pattern p = Pattern.compile(pattern);
		//Matcher
		Matcher m = p.matcher(text);
		//Arreglo para ir guardando los substrings en orden
		ArrayList<String> l = new ArrayList<String>();
		String noTag;

		int[] previo = { 0, 0 };
		int[] actual = { 0, 0 };

		while (m.find()) {
			actual[0] = m.start();
			actual[1] = m.end();

			if (actual[0] > previo[1]) { //Si hay algun texto entre 2 matchs
				noTag = text.substring(previo[1], actual[0]);
				l.add(analize(noTag));
			}

			l.add(m.group());
			previo[0] = actual[0];
			previo[1] = actual[1];
		}

		if (text.length() > actual[1]) {//Si hay algun texto depues del ultimo
										//match
			noTag = text.substring(actual[1], text.length());
			l.add(analize(noTag));
		}

		//Reconstruccion
		String s = "";
		for (String st : l) {
			s += st;
		}
		return s;

	}

	/**
	 * Metodo que analiza una palabra y la retorna modificada seg�n la funci�n
	 * modify(String)
	 * @param msg String a analizar
	 * @return String modificado
	 */
	private String analize(String msg) {
		if (condition(msg)) {
			msg = modify(msg);
		}
		return msg;
	}

	/**
	 * M�todo abstracto que denota la condici�n que debe cumplir el String msg
	 * para ser modificado por el Proxy
	 * 
	 * @param msg  String sobre el cual se debe verificar la condici�n
	 * @return boolean que denota si se cumple o no la condici�n
	 */
	public abstract boolean condition(String msg);

	/**
	 * M�todo abstracto que modifica el String msg por otro String seg�n el tipo
	 * de Proxy
	 * 
	 * @param msg String a modificar
	 * @return String modificado
	 */
	public abstract String modify(String msg);

}
