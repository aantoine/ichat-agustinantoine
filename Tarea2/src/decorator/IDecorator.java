package decorator;

/**
 * Interfaz para los decoradores
 * @author Agustin Antoine
 *
 */
public interface IDecorator {
	/**
	 * Metodo que verifica si el String de entrada cumple las condiciones para
	 * ser modificado
	 * @param msg es el String sobre el cual verificar la condicion
	 * @return es el Boolean que determina si cunple o no la condicion
	 */
	public boolean condition(String msg);
	
	/**
	 * Metodo que permite modificar un String de entrada segun el decorador
	 * @param msg es el String a modificar
	 * @return es el String Modificado
	 */
	public String modify(String msg);
}
