package decorator;

import proxy.IProxy;

public class HLDecorator extends AbstractDecorator {

	public HLDecorator(IProxy n) {
		super(n);
	}
	
	@Override
	public boolean condition(String msg) {
		return (msg.charAt(0)=='@' || msg.charAt(0)=='#');
	}

	@Override
	public String modify(String msg) {
		return "<em>"+msg+"</em>";
	}


}
