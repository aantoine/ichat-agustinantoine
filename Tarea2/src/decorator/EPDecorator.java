package decorator;

import proxy.IProxy;

/**
 * Proxy que permite agregar facilmente una imagen en formato html
 * @author Agustin Antoine
 *
 */
public class EPDecorator extends AbstractDecorator {
	
	/**
	 * Constructor de la clase
	 * @param n Decorador previo
	 */
	public EPDecorator(IProxy n) {
		super(n);
	}

	/**
	 * Metodo que verifica si la palabra posee la sintaxis adecuada
	 * @param msg String sobre el cual computar la condicion.
	 * @return Boolean True si cumple la condicion
	 */
	@Override
	public boolean condition(String msg) {
		int size = msg.length();
		return (msg.charAt(0)=='$' && msg.charAt(size-1)=='$');
		
	}

	/**
	 * Metodo que reemplaza la palabra por el tag html que permite
	 * mostrar la imagen
	 * @param msg String al que agregar los tag
	 * @return String con los tag html para mostrar una foto 
	 */
	@Override
	public String modify(String msg) {
		msg=msg.substring(1, msg.length()-1);
				
		return "<img src=\"file:images/"+msg+"\" width=50 height=50/>";
	}

}
