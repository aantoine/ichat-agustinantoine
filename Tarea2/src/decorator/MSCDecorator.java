package decorator;

import proxy.IProxy;
/**
 * Proxy que verifica si la palabra esta bien escrita o no
 * @author Agustin Antoine
 *
 */
public class MSCDecorator extends AbstractChecker {
	
	/**
	 * Construcor de la clase
	 * @param n Proxy previo
	 */
	public MSCDecorator(IProxy n){
		super(n);
		addToList("red");
		addToList("blue");
		addToList("black");
		addToList("hola");
		addToList("como");
		addToList("estas");
		addToList("perro");
		addToList("castillo");
		addToList("casa");
		addToList("gato");
		addToList("coraz�n");
		addToList("camisa");
		addToList("cartas");
		addToList("la");
		addToList("el");
		addToList("un");
		addToList("una");
		addToList("maleta");
		addToList("azucar");
	}
	
	/**
	 * Verifica si el String msg no est� en el diccionario 
	 * @param msg String a verificar
	 * @return boolean, True si no est� en el diccionario
	 */
	@Override
	public boolean condition(String msg) {
		return !super.condition(msg);
	}

	/**
	 * Modifica el texto subrayandolo y escribi�ndolo en rojo
	 * @param msg String a modificar
	 * @return String modificado
	 */
	@Override
	public String modify(String msg) {
		return "<u><font color=\"red\">"+msg+"</font></u>";
	}

}
