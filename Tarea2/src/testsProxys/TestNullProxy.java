package testsProxys;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import proxy.IProxy;
import proxy.NullProxy;



public class TestNullProxy {
	IProxy proxy;
	
	
	@Before public void setUp() { // create the test data
		proxy = new NullProxy();
	}

	@Test
	public void testPreProcess() {
		String expected = "Processed test";
		assertEquals(expected, proxy.preProcess(expected));
		
		expected = "<b>Processed test</b>";
		assertEquals(expected, proxy.preProcess(expected));
	}

}
