package testsProxys;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import proxy.AbstractProxy;
import proxy.IProxy;
import proxy.NullProxy;
import proxy.SplitMsgProxy;

public class TestSplitProxy {
	class GetProxy extends AbstractProxy {
		private String actual;
		
		public GetProxy(IProxy prev) {
			super(prev);
			actual="null";
		}	

		@Override
		public String preProcess(String msg) {
			actual=msg;
			return msg;
		}
		
		public String getter(){
			return actual;
		}


	}

	IProxy proxy;
	GetProxy get;

	@Before
	public void setUp() { // create the test data
		get = new GetProxy(new NullProxy());
		proxy = new SplitMsgProxy(get);
	}

	@Test
	public void testSeverMessage() {
		String msg = "juampi has joined";
		String retorno = proxy.preProcess(msg);
		String resultado = get.getter();
		assertEquals(msg, retorno);
		assertEquals("null", resultado);

	}
	
	@Test
	public void testClientMessage() {
		String msg = "agustin - Hola como estas?"; 
		String retorno = proxy.preProcess(msg);
		String resultado = get.getter();
		
		assertEquals(msg, retorno);
		assertEquals(" Hola como estas?", resultado);

	}

}
