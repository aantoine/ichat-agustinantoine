package testsProxys;

import static org.junit.Assert.*;
import java.io.IOException;
import java.io.PrintStream;
import org.junit.Before;
import org.junit.Test;
import proxy.IProxy;
import proxy.LogProxy;
import proxy.NullProxy;

class TestPrinter extends PrintStream{
	
	private String actual;		
	
	public TestPrinter(PrintStream out)  {
		super(out);
	}
	
	@Override
	public void println(String text){
		actual=text;
	}
	public String getActual(){
		return actual;
	}

}

public class TestLogProxy {
	
	LogProxy proxy;
	TestPrinter printer;
	

	@Before
	public void setUp() throws IOException { // create the test data
		IProxy n = new NullProxy();
		proxy = new LogProxy(n);
		printer = new TestPrinter(System.out);
		
	}

	@Test
	public void testPreProcess() {
		String original = "agustin - Hola como estas?"; 
		String log = "agustin , Hola como estas?, "+(new java.util.Date().toString());
		proxy.append(printer, original);
		assertEquals(log, printer.getActual());
		
	}
	
	@Test
	public void testPreProcess2() {
	}
	
	@Test
	public void testPreProcessTags() {
	}

}
